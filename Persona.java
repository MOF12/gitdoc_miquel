/** Classe Persona destinada a modificar els seus atributs segons la utilització dels seus mètodes
* @author Miquel Oliver
* @version 1.0.0
* @since 1.0.0
*/
public class Persona {

public String nom;
public String cognom;

private final String nomComplet;
private float posicio;

/** Constructor de la classe persona a partir de nom i cognom
*/
public Persona(String _nom, String _cognom) {
nomComplet = nom + " " + cognom;
nom = _nom;
cognom = _cognom;
}

public void caminar(float _distancia) {
posicio += moure(_distancia);
}

protected float moure(float _distancia) {
return _distancia * 1.0f;
}

public void parlar(String _missatge) {
System.out.println(generarMissatge(_missatge));
}

private String generarMissatge(String _missatge) {
return nomComplet + " (" + posicio + "m): " + _missatge;
}
}
